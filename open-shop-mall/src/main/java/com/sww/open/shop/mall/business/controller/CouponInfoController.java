package com.sww.open.shop.mall.business.controller;

import com.sww.open.shop.mall.business.entity.CouponInfo;
import com.sww.open.shop.mall.business.service.CouponInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 * 优惠卷表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@RestController
@RequestMapping("/business/couponinfo")
public class CouponInfoController {

    @Autowired
    private CouponInfoService couponInfoService;

    /***
     * 查询一条数据
     * @param entity
     * @return
     */
    @RequestMapping(path = "/get", method = RequestMethod.POST)
    public Object query(@RequestBody CouponInfo entity) {

        return null;
    }

    /***
     * 新增数据
     * @param entity
     * @return
     */
    @RequestMapping(path = "/add", method = RequestMethod.POST)
    public Object add(@RequestBody CouponInfo entity) {

        return null;
    }

    /**
     * 更新数据
     *
     * @param entity
     * @return
     */
    @RequestMapping(path = "/update", method = RequestMethod.POST)
    public Object update(@RequestBody CouponInfo entity) {

        return null;
    }
}
