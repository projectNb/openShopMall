package com.sww.open.shop.mall.business.mapper;

import com.sww.open.shop.mall.business.entity.OrderInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 订单产品表
 *
 * @author sww
 * @date 2021-08-15 20:11:51
 */
@Mapper
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {
	
}
