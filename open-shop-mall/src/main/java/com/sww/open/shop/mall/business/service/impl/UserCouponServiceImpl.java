package com.sww.open.shop.mall.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sww.open.shop.mall.business.mapper.UserCouponMapper;
import com.sww.open.shop.mall.business.entity.UserCoupon;
import com.sww.open.shop.mall.business.service.UserCouponService;
import org.springframework.stereotype.Service;


/**
 * 用户优惠卷表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@Service("userCouponService")
public class UserCouponServiceImpl extends ServiceImpl<UserCouponMapper, UserCoupon> implements UserCouponService {

}
