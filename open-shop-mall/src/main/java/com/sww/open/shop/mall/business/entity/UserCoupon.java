package com.sww.open.shop.mall.business.entity;

import lombok.*;
import java.util.Date;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.*;


/**
 * 用户优惠卷表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@TableName("tb_user_coupon")
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class UserCoupon implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户优惠id
	 */
    @TableId
	@TableField("user_coupon_id")
    private Long userCouponId;

	/**
	 * 用户ID
	 */
	@TableField("user_id")
    private String userId;

	/**
	 * 店铺id
	 */
	@TableField("shop_id")
    private Long shopId;

	/**
	 * 用户名称
	 */
	@TableField("user_name")
    private String userName;

	/**
	 * 优惠卷ID
	 */
	@TableField("coupo_id")
    private Long coupoId;

	/**
	 * 0正常、1失效
	 */
	@TableField("is_delete")
    private boolean isDelete;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
    private Date createTime;

}
