package com.sww.open.shop.mall.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sww.open.shop.mall.sys.mapper.UserMapper;
import com.sww.open.shop.mall.sys.entity.User;
import com.sww.open.shop.mall.sys.service.UserService;
import org.springframework.stereotype.Service;


/**
 * 
 *
 * @author sww
 * @date 2021-08-15 19:53:13
 */
@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
