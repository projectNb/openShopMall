package com.sww.open.shop.mall.config.mybstis;


import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * 分页插件
 * cp: 来源自 https://mp.baomidou.com
 *
 * @author sww
 * @version 1.0.0
 * @date 2021/8/15
 */
@Configuration
public class MybatisPlusInterceptorConfig {

    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {

        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();

        interceptor.addInnerInterceptor(new PaginationInnerInterceptor());

        // 如果是不同类型的库，请不要指定DbType，其会自动判断。
        // interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }
}