package com.sww.open.shop.mall.business.controller;

import com.sww.open.shop.mall.business.entity.UserInfo;
import com.sww.open.shop.mall.business.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 * 用户表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@RestController
@RequestMapping("/business/userinfo")
public class UserInfoController {

    @Autowired
    private UserInfoService userInfoService;

    /***
     * 查询一条数据
     * @param entity
     * @return
     */
    @RequestMapping(path = "/get", method = RequestMethod.POST)
    public Object query(@RequestBody UserInfo entity) {

        return null;
    }

    /***
     * 新增数据
     * @param entity
     * @return
     */
    @RequestMapping(path = "/add", method = RequestMethod.POST)
    public Object add(@RequestBody UserInfo entity) {

        return null;
    }

    /**
     * 更新数据
     *
     * @param entity
     * @return
     */
    @RequestMapping(path = "/update", method = RequestMethod.POST)
    public Object update(@RequestBody UserInfo entity) {

        return null;
    }
}
