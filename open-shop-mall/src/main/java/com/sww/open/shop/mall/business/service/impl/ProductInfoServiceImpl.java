package com.sww.open.shop.mall.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sww.open.shop.mall.business.mapper.ProductInfoMapper;
import com.sww.open.shop.mall.business.entity.ProductInfo;
import com.sww.open.shop.mall.business.service.ProductInfoService;
import org.springframework.stereotype.Service;


/**
 * 餐品表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@Service("productInfoService")
public class ProductInfoServiceImpl extends ServiceImpl<ProductInfoMapper, ProductInfo> implements ProductInfoService {

}
