package com.sww.open.shop.mall.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sww.open.shop.mall.business.mapper.ProductSkuMapper;
import com.sww.open.shop.mall.business.entity.ProductSku;
import com.sww.open.shop.mall.business.service.ProductSkuService;
import org.springframework.stereotype.Service;


/**
 * 餐品sku表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@Service("productSkuService")
public class ProductSkuServiceImpl extends ServiceImpl<ProductSkuMapper, ProductSku> implements ProductSkuService {

}
