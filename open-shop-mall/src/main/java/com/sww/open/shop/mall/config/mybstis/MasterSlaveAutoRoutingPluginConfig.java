package com.sww.open.shop.mall.config.mybstis;/**
 * @description TODO
 * @date 2021/8/15 2:04
 * @author Willie.shi
 */

import com.baomidou.dynamic.datasource.plugin.MasterSlaveAutoRoutingPlugin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自动读写分离配置
 * cp: 来源自 https://mp.baomidou.com
 *
 * @author sww
 * @version v1.0.0
 * @date 2021/8/15
 */
@Configuration
public class MasterSlaveAutoRoutingPluginConfig {

    @Bean
    public MasterSlaveAutoRoutingPlugin masterSlaveAutoRoutingPlugin() {
        return new MasterSlaveAutoRoutingPlugin();
    }
}
