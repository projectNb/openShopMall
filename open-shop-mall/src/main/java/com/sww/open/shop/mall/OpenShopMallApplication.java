package com.sww.open.shop.mall;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author sww
 * @version v1.0.0
 * @date 2021/8/14 4:56
 */
@SpringBootApplication
@MapperScan({"com.sww.open.shop.mall.business.mapper","com.sww.open.shop.mall.sys.mapper"})
public class OpenShopMallApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenShopMallApplication.class, args);
    }

}
