package com.sww.open.shop.mall.business.entity;

import lombok.*;
import java.util.Date;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.*;


/**
 * 餐品sku表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@TableName("tb_product_sku")
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ProductSku implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 餐品内容ID
	 */
    @TableId
	@TableField("sku_id")
    private Long skuId;

	/**
	 * 餐品ID
	 */
	@TableField("product_id")
    private Long productId;

	/**
	 * 餐品sku内容名称
	 */
	@TableField("sku_mame")
    private String skuMame;

	/**
	 * 是否顶级(是顶级则联动下级sku,类名展示)，0是，1否
	 */
	@TableField("is_father")
    private boolean isFather;

	/**
	 * 店铺id
	 */
	@TableField("shop_id")
    private Long shopId;

	/**
	 * 打包盒数量
	 */
	@TableField("box_num")
    private Integer boxNum;

	/**
	 * 打包盒价格
	 */
	@TableField("box_price")
    private double boxPrice;

	/**
	 * sku是否固定(是固定则前端不展示)，0是，1否
	 */
	@TableField("is_fixed")
    private boolean isFixed;

	/**
	 * 餐品上下架状态（用户端），0贩卖，1售馨
	 */
	@TableField("is_sold_out")
    private boolean isSoldOut;

	/**
	 * 分类排序: 数字越小、排名越靠前
	 */
	@TableField("picture_sequence")
    private Long pictureSequence;

	/**
	 * 餐品贩卖时间段(可改)
	 */
	@TableField("sku_time")
    private String skuTime;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
    private Date createTime;

	/**
	 * 创建人
	 */
	@TableField("create_by")
    private String createBy;

	/**
	 * 更新时间
	 */
	@TableField("update_time")
    private Date updateTime;

	/**
	 * 更新人
	 */
	@TableField("update_by")
    private String updateBy;

	/**
	 * 0正常、1已被删除
	 */
	@TableField("is_delete")
    private boolean isDelete;

}
