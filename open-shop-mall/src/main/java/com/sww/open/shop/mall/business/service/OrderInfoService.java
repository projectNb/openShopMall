package com.sww.open.shop.mall.business.service;

import com.sww.open.shop.mall.business.entity.OrderInfo;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * 订单产品表
 *
 * @author sww
 * @date 2021-08-15 20:11:51
 */
public interface OrderInfoService extends IService<OrderInfo> {

}

