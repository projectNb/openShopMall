package com.sww.open.shop.mall.business.entity;

import lombok.*;
import java.util.Date;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.*;


/**
 * 用户表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@TableName("tb_user_info")
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class UserInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户id
	 */
    @TableId
	@TableField("user_id")
    private Long userId;

	/**
	 * 用户名称
	 */
	@TableField("user_name")
    private String userName;

	/**
	 * 密码
	 */
	@TableField("user_password")
    private String userPassword;

	/**
	 * 盐
	 */
	@TableField("user_salt")
    private String userSalt;

	/**
	 * 邮箱
	 */
	@TableField("user_email")
    private String userEmail;

	/**
	 * 手机号
	 */
	@TableField("user_mobile")
    private String userMobile;

	/**
	 * 状态 0禁用 1正常
	 */
	@TableField("user_status")
    private boolean userStatus;

	/**
	 * 是否管理员，0否，1是
	 */
	@TableField("is_sys_admin")
    private boolean isSysAdmin;

	/**
	 * 微信id
	 */
	@TableField("wechat_id")
    private String wechatId;

	/**
	 * 是否有代办，0否，1是
	 */
	@TableField("is_agency")
    private boolean isAgency;

	/**
	 * 是否禁用推送代办，0否，1是
	 */
	@TableField("is_push")
    private boolean isPush;

	/**
	 * 是否有待评论，0否，1是
	 */
	@TableField("is_comment")
    private boolean isComment;

	/**
	 * 购物车是否有物品，0否，1是
	 */
	@TableField("is_shop_cart")
    private boolean isShopCart;

	/**
	 * 是否待付款，0否，1是
	 */
	@TableField("is_payment")
    private boolean isPayment;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
    private Date createTime;

	/**
	 * 创建人
	 */
	@TableField("create_by")
    private String createBy;

	/**
	 * 更新时间
	 */
	@TableField("update_time")
    private Date updateTime;

	/**
	 * 更新人
	 */
	@TableField("update_by")
    private String updateBy;

	/**
	 * 0正常、1已被删除
	 */
	@TableField("is_delete")
    private boolean isDelete;

}
