package com.sww.open.shop.mall.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sww.open.shop.mall.business.mapper.OrderInfoMapper;
import com.sww.open.shop.mall.business.entity.OrderInfo;
import com.sww.open.shop.mall.business.service.OrderInfoService;
import org.springframework.stereotype.Service;


/**
 * 订单产品表
 *
 * @author sww
 * @date 2021-08-15 20:11:51
 */
@Service("orderInfoService")
public class OrderInfoServiceImpl extends ServiceImpl<OrderInfoMapper, OrderInfo> implements OrderInfoService {

}
