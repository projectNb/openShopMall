package com.sww.open.shop.mall.business.entity;

import lombok.*;
import java.util.Date;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.*;


/**
 * 优惠卷表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@TableName("tb_coupon_info")
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class CouponInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 优惠id
	 */
    @TableId
	@TableField("coupo_id")
    private Long coupoId;

	/**
	 * 用户名称
	 */
	@TableField("coupo_name")
    private String coupoName;

	/**
	 * 优惠卷类型：0满减、1满送、2餐品优惠
	 */
	@TableField("coupo_type")
    private boolean coupoType;

	/**
	 * 餐品ID
	 */
	@TableField("product_id")
    private Long productId;

	/**
	 * 0正常、1已被删除
	 */
	@TableField("is_delete")
    private boolean isDelete;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
    private Date createTime;

	/**
	 * 创建人
	 */
	@TableField("create_by")
    private String createBy;

	/**
	 * 更新时间
	 */
	@TableField("update_time")
    private Date updateTime;

	/**
	 * 更新人
	 */
	@TableField("update_by")
    private String updateBy;

}
