package com.sww.open.shop.mall.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sww.open.shop.mall.business.mapper.CouponInfoMapper;
import com.sww.open.shop.mall.business.entity.CouponInfo;
import com.sww.open.shop.mall.business.service.CouponInfoService;
import org.springframework.stereotype.Service;


/**
 * 优惠卷表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@Service("couponInfoService")
public class CouponInfoServiceImpl extends ServiceImpl<CouponInfoMapper, CouponInfo> implements CouponInfoService {

}
