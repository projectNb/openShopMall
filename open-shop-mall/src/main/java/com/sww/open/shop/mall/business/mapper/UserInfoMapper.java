package com.sww.open.shop.mall.business.mapper;

import com.sww.open.shop.mall.business.entity.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 用户表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@Mapper
public interface UserInfoMapper extends BaseMapper<UserInfo> {
	
}
