package com.sww.open.shop.mall.business.mapper;

import com.sww.open.shop.mall.business.entity.UserCart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 用户购物车表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@Mapper
public interface UserCartMapper extends BaseMapper<UserCart> {
	
}
