package com.sww.open.shop.mall.business.mapper;

import com.sww.open.shop.mall.business.entity.ProductSku;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 餐品sku表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@Mapper
public interface ProductSkuMapper extends BaseMapper<ProductSku> {
	
}
