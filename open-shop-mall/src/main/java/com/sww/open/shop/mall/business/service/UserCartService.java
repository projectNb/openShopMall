package com.sww.open.shop.mall.business.service;

import com.sww.open.shop.mall.business.entity.UserCart;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * 用户购物车表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
public interface UserCartService extends IService<UserCart> {

}

