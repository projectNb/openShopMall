package com.sww.open.shop.mall.business.entity;

import lombok.*;
import java.util.Date;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.*;


/**
 * 订单产品表
 *
 * @author sww
 * @date 2021-08-15 20:11:51
 */
@TableName("tb_order_info")
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class OrderInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键ID
	 */
    @TableId
	@TableField("id")
    private String id;

	/**
	 * 品牌ID
	 */
	@TableField("brand_id")
    private Integer brandId;

	/**
	 * 品牌名称
	 */
	@TableField("brand_name")
    private String brandName;

	/**
	 * 店铺id
	 */
	@TableField("shop_id")
    private Long shopId;

	/**
	 * 店铺名称
	 */
	@TableField("shop_name")
    private String shopName;

	/**
	 * 城市KEY
	 */
	@TableField("city_code")
    private String cityCode;

	/**
	 * 城市名
	 */
	@TableField("city_name")
    private String cityName;

	/**
	 * 门店所在区域编码
	 */
	@TableField("city_county_code")
    private String cityCountyCode;

	/**
	 * 0-无、100门店骑手
	 */
	@TableField("delivery_type")
    private boolean deliveryType;

	/**
	 * 第三方配送状态:0-呼叫中;2-待接单;3-已确认;5-已分配;7-配送中;10-已送达;15-已取消;-1-失败;-2-异常;
	 */
	@TableField("delivery_status")
    private boolean deliveryStatus;

	/**
	 * 呼叫第三方失败次数
	 */
	@TableField("delivery_push_fail_count")
    private Integer deliveryPushFailCount;

	/**
	 * 呼叫第三方失败响应消息
	 */
	@TableField("delivery_push_fail_msg")
    private String deliveryPushFailMsg;

	/**
	 * 订单类型:(eatIn堂食),(takeOut外卖)
	 */
	@TableField("order_type")
    private String orderType;

	/**
	 * 订单来源
	 */
	@TableField("order_source")
    private String orderSource;

	/**
	 * 订单预送达时间
	 */
	@TableField("expect_delivery_time")
    private Date expectDeliveryTime;

	/**
	 * 接单时间
	 */
	@TableField("accpect_time")
    private Date accpectTime;

	/**
	 * 预约单确定时间:呼叫第三方配送、自配送显示未分配列表时间
	 */
	@TableField("reserva_confirm_time")
    private Date reservaConfirmTime;

	/**
	 * 停单时间
	 */
	@TableField("stop_time")
    private Date stopTime;

	/**
	 * 完成/取消时间
	 */
	@TableField("finish_time")
    private Date finishTime;

	/**
	 * 停单状态：0-未停单，1-已停单
	 */
	@TableField("stop_status")
    private boolean stopStatus;

	/**
	 * 订单状态：1-待接单;5-待汇集;10-汇集中;15-配送中;100-配送完成;-1-已取消;-2-门店停单;
	 */
	@TableField("order_status")
    private boolean orderStatus;

	/**
	 * 0-未打印，1-已打印,2-取消打印
	 */
	@TableField("print_status")
    private boolean printStatus;

	/**
	 * 取消订单原因内容
	 */
	@TableField("cancell_reason")
    private String cancellReason;

	/**
	 * 配送公司ID
	 */
	@TableField("deliver_company_id")
    private Integer deliverCompanyId;

	/**
	 * 配送区域团队ID
	 */
	@TableField("deliver_region_id")
    private Integer deliverRegionId;

	/**
	 * 配送区域团队名称
	 */
	@TableField("deliver_region_name")
    private String deliverRegionName;

	/**
	 * 礼品数据
	 */
	@TableField("product_gift_items")
    private String productGiftItems;

	/**
	 * 收货人地址经度, 如果无,传0.(高德地图坐标)
	 */
	@TableField("receiver_lng")
    private Double receiverLng;

	/**
	 * 收货人地址纬度, 如果无,传0.(高德地图坐标)
	 */
	@TableField("receiver_lat")
    private Double receiverLat;

	/**
	 * 用户名
	 */
	@TableField("consignee_name")
    private String consigneeName;

	/**
	 * 用户电话号码
	 */
	@TableField("consignee_phone")
    private String consigneePhone;

	/**
	 * 联系地址
	 */
	@TableField("consignee_address")
    private String consigneeAddress;

	/**
	 * 订单号
	 */
	@TableField("order_no")
    private String orderNo;

	/**
	 * 订单备注
	 */
	@TableField("order_remarks")
    private String orderRemarks;

	/**
	 * 是即时交货
	 */
	@TableField("is_immediate_delivery")
    private boolean isImmediateDelivery;

	/**
	 * 是否修改订单
	 */
	@TableField("is_modified")
    private boolean isModified;

	/**
	 * 订单创建时间
	 */
	@TableField("order_create_time")
    private Date orderCreateTime;

	/**
	 * 支付类型
	 */
	@TableField("tender_type")
    private String tenderType;

	/**
	 * 是否已经支付：0-未支付，1-已支付
	 */
	@TableField("is_payed")
    private boolean isPayed;

	/**
	 * 订单总金额(单位：分)
	 */
	@TableField("gross_total")
    private Integer grossTotal;

	/**
	 * 订单补贴金额
	 */
	@TableField("subsidy")
    private Integer subsidy;

	/**
	 * 餐单金额(单位：分)
	 */
	@TableField("product_price")
    private Integer productPrice;

	/**
	 * 运送费(单位：分)
	 */
	@TableField("deliveryfee")
    private Integer deliveryfee;

	/**
	 * 停单标记，0 未停单 1 已停单
	 */
	@TableField("is_stop")
    private boolean isStop;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
    private Date createTime;

	/**
	 * 最后修改时间
	 */
	@TableField("update_time")
    private Date updateTime;

}
