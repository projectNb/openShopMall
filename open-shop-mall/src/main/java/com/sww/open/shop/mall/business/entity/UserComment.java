package com.sww.open.shop.mall.business.entity;

import lombok.*;
import java.util.Date;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.*;


/**
 * 用户评论表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@TableName("tb_user_comment")
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class UserComment implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户评论id
	 */
    @TableId
	@TableField("comment_id")
    private Long commentId;

	/**
	 * 用户ID
	 */
	@TableField("user_id")
    private String userId;

	/**
	 * 用户名称
	 */
	@TableField("user_name")
    private String userName;

	/**
	 * 订单ID
	 */
	@TableField("order_id")
    private String orderId;

	/**
	 * 餐品ID
	 */
	@TableField("product_id")
    private Long productId;

	/**
	 * 评论标题
	 */
	@TableField("article_title")
    private String articleTitle;

	/**
	 * 评论等级[ 1差，2好评，3差评]
	 */
	@TableField("comment_level")
    private boolean commentLevel;

	/**
	 * 评论的内容
	 */
	@TableField("comment_content")
    private String commentContent;

	/**
	 * 0正常、1已被删除
	 */
	@TableField("is_delete")
    private boolean isDelete;

	/**
	 * 是否系统：0否、1是
	 */
	@TableField("is_sys_del")
    private boolean isSysDel;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
    private Date createTime;

	/**
	 * 创建人
	 */
	@TableField("create_by")
    private String createBy;

	/**
	 * 更新时间
	 */
	@TableField("update_time")
    private Date updateTime;

	/**
	 * 更新人
	 */
	@TableField("update_by")
    private String updateBy;

}
