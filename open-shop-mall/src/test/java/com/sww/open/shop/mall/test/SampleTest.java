package com.sww.open.shop.mall.test;


import com.sww.open.shop.mall.sys.entity.User;
import com.sww.open.shop.mall.sys.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * TODO
 *
 * @author willie
 * @version TODO
 * @date 2021/8/15
 */
@SpringBootTest
public class SampleTest {

    @Autowired
    private UserService userService;

    @Test
    public void testSelect() {

        System.out.println(("----- selectAll method test ------"));
        List<User> userList = userService.list();
        userList.forEach(System.out::println);

        User user = new User();
        user.setId(1001L);
        user.setAge(29);
        user.setEmail("1764441651@qq.com");
        user.setName("willie");
        userService.save(user);
    }
}