package com.sww.open.shop.mall.sys.service;

import com.sww.open.shop.mall.sys.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * 
 *
 * @author sww
 * @date 2021-08-15 19:53:13
 */
public interface UserService extends IService<User> {

}

