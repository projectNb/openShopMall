package com.sww.open.shop.mall.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sww.open.shop.mall.business.mapper.UserInfoMapper;
import com.sww.open.shop.mall.business.entity.UserInfo;
import com.sww.open.shop.mall.business.service.UserInfoService;
import org.springframework.stereotype.Service;


/**
 * 用户表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@Service("userInfoService")
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {

}
