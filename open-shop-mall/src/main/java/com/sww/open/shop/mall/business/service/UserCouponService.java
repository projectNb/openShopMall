package com.sww.open.shop.mall.business.service;

import com.sww.open.shop.mall.business.entity.UserCoupon;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * 用户优惠卷表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
public interface UserCouponService extends IService<UserCoupon> {

}

