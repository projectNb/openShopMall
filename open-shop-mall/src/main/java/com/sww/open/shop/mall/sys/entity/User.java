package com.sww.open.shop.mall.sys.entity;

import lombok.*;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.*;


/**
 * 
 *
 * @author sww
 * @date 2021-08-15 19:53:13
 */
@TableName("user")
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键ID
	 */
    @TableId
	@TableField("id")
    private Long id;

	/**
	 * 姓名
	 */
	@TableField("name")
    private String name;

	/**
	 * 年龄
	 */
	@TableField("age")
    private Integer age;

	/**
	 * 邮箱
	 */
	@TableField("email")
    private String email;

}
