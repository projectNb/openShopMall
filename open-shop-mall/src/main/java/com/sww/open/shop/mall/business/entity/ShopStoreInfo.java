package com.sww.open.shop.mall.business.entity;

import lombok.*;
import java.util.Date;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.*;


/**
 * 店铺表
 *
 * @author sww
 * @date 2021-08-15 20:11:51
 */
@TableName("tb_shop_store_info")
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ShopStoreInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 店铺id
	 */
    @TableId
	@TableField("shop_id")
    private Long shopId;

	/**
	 * 店铺名称(可改)、不唯一
	 */
	@TableField("shop_name")
    private String shopName;

	/**
	 * 店铺类型(0外卖、1堂食、2混合)
	 */
	@TableField("shop_type")
    private boolean shopType;

	/**
	 * 店铺简介(可改)
	 */
	@TableField("intro")
    private String intro;

	/**
	 * 店铺公告(可改)
	 */
	@TableField("shop_notice")
    private String shopNotice;

	/**
	 * 店铺行业(餐饮、果蔬、生鲜等)
	 */
	@TableField("shop_industry_type")
    private boolean shopIndustryType;

	/**
	 * 店铺联系电话
	 */
	@TableField("mobile_phone")
    private String mobilePhone;

	/**
	 * 店铺所在纬度(可改)
	 */
	@TableField("shop_lat")
    private String shopLat;

	/**
	 * 店铺所在经度(可改)
	 */
	@TableField("shop_lng")
    private String shopLng;

	/**
	 * 店铺详细地址(可改)
	 */
	@TableField("shop_address")
    private String shopAddress;

	/**
	 * 店铺所在省份
	 */
	@TableField("province")
    private String province;

	/**
	 * 店铺所在城市
	 */
	@TableField("city")
    private String city;

	/**
	 * 店铺所在区域
	 */
	@TableField("area")
    private String area;

	/**
	 * 店铺省市区代码，用于回显
	 */
	@TableField("pca_code")
    private String pcaCode;

	/**
	 * 店铺logo(可改)
	 */
	@TableField("shop_logo")
    private String shopLogo;

	/**
	 * 店铺图片(可改)
	 */
	@TableField("shop_photos")
    private String shopPhotos;

	/**
	 * 每天营业时间段(可改)
	 */
	@TableField("open_time")
    private String openTime;

	/**
	 * 店铺状态(-1:未上线 0:休息 1:营业)可改
	 */
	@TableField("shop_status")
    private boolean shopStatus;

	/**
	 * 固定运费(可改)
	 */
	@TableField("fixed_freight")
    private double fixedFreight;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
    private Date createTime;

	/**
	 * 创建人
	 */
	@TableField("create_by")
    private String createBy;

	/**
	 * 更新时间
	 */
	@TableField("update_time")
    private Date updateTime;

	/**
	 * 更新人
	 */
	@TableField("update_by")
    private String updateBy;

	/**
	 * 0正常、1已被删除
	 */
	@TableField("is_delete")
    private boolean isDelete;

}
