package com.sww.open.shop.mall.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sww.open.shop.mall.business.mapper.QrCodeInfoMapper;
import com.sww.open.shop.mall.business.entity.QrCodeInfo;
import com.sww.open.shop.mall.business.service.QrCodeInfoService;
import org.springframework.stereotype.Service;


/**
 * 门店二维码表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@Service("qrCodeInfoService")
public class QrCodeInfoServiceImpl extends ServiceImpl<QrCodeInfoMapper, QrCodeInfo> implements QrCodeInfoService {

}
