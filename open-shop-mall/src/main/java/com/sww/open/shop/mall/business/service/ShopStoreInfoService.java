package com.sww.open.shop.mall.business.service;

import com.sww.open.shop.mall.business.entity.ShopStoreInfo;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * 店铺表
 *
 * @author sww
 * @date 2021-08-15 20:11:51
 */
public interface ShopStoreInfoService extends IService<ShopStoreInfo> {

}

