package com.sww.open.shop.mall.business.service;

import com.sww.open.shop.mall.business.entity.ProductSku;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * 餐品sku表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
public interface ProductSkuService extends IService<ProductSku> {

}

