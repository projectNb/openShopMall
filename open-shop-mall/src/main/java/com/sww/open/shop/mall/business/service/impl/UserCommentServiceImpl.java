package com.sww.open.shop.mall.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sww.open.shop.mall.business.mapper.UserCommentMapper;
import com.sww.open.shop.mall.business.entity.UserComment;
import com.sww.open.shop.mall.business.service.UserCommentService;
import org.springframework.stereotype.Service;


/**
 * 用户评论表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@Service("userCommentService")
public class UserCommentServiceImpl extends ServiceImpl<UserCommentMapper, UserComment> implements UserCommentService {

}
