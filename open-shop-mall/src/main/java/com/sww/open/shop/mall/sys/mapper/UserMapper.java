package com.sww.open.shop.mall.sys.mapper;

import com.sww.open.shop.mall.sys.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 
 *
 * @author sww
 * @date 2021-08-15 19:53:13
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
	
}
