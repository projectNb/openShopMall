package com.sww.open.shop.mall.business.mapper;

import com.sww.open.shop.mall.business.entity.ProductCategoryInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 餐品分类表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@Mapper
public interface ProductCategoryInfoMapper extends BaseMapper<ProductCategoryInfo> {
	
}
