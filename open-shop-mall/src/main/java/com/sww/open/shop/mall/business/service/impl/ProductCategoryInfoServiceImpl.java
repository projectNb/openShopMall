package com.sww.open.shop.mall.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sww.open.shop.mall.business.mapper.ProductCategoryInfoMapper;
import com.sww.open.shop.mall.business.entity.ProductCategoryInfo;
import com.sww.open.shop.mall.business.service.ProductCategoryInfoService;
import org.springframework.stereotype.Service;


/**
 * 餐品分类表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@Service("productCategoryInfoService")
public class ProductCategoryInfoServiceImpl extends ServiceImpl<ProductCategoryInfoMapper, ProductCategoryInfo> implements ProductCategoryInfoService {

}
