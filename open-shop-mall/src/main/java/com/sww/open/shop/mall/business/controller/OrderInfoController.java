package com.sww.open.shop.mall.business.controller;

import com.sww.open.shop.mall.business.entity.OrderInfo;
import com.sww.open.shop.mall.business.service.OrderInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 * 订单产品表
 *
 * @author sww
 * @date 2021-08-15 20:11:51
 */
@RestController
@RequestMapping("/business/orderinfo")
public class OrderInfoController {

    @Autowired
    private OrderInfoService orderInfoService;

    /***
     * 查询一条数据
     * @param entity
     * @return
     */
    @RequestMapping(path = "/get", method = RequestMethod.POST)
    public Object query(@RequestBody OrderInfo entity) {

        return null;
    }

    /***
     * 新增数据
     * @param entity
     * @return
     */
    @RequestMapping(path = "/add", method = RequestMethod.POST)
    public Object add(@RequestBody OrderInfo entity) {

        return null;
    }

    /**
     * 更新数据
     *
     * @param entity
     * @return
     */
    @RequestMapping(path = "/update", method = RequestMethod.POST)
    public Object update(@RequestBody OrderInfo entity) {

        return null;
    }
}
