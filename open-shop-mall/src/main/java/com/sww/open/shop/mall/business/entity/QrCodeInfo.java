package com.sww.open.shop.mall.business.entity;

import lombok.*;
import java.util.Date;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.*;


/**
 * 门店二维码表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@TableName("tb_qr_code_info")
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class QrCodeInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 二维码ID
	 */
    @TableId
	@TableField("qr_code_id")
    private Long qrCodeId;

	/**
	 * 二维码类型(0:门店地址、1:桌号)
	 */
	@TableField("qr_code_type")
    private boolean qrCodeType;

	/**
	 * 二维码名称(可改)
	 */
	@TableField("qr_code_name")
    private String qrCodeName;

	/**
	 * 店铺id
	 */
	@TableField("shop_id")
    private Long shopId;

	/**
	 * 真实地址
	 */
	@TableField("real_ip_address")
    private String realIpAddress;

	/**
	 * 显示地址(短链接)
	 */
	@TableField("show_ip_address")
    private String showIpAddress;

	/**
	 * 0禁用、1启用
	 */
	@TableField("qr_code_status")
    private boolean qrCodeStatus;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
    private Date createTime;

	/**
	 * 创建人
	 */
	@TableField("create_by")
    private String createBy;

	/**
	 * 更新时间
	 */
	@TableField("update_time")
    private Date updateTime;

	/**
	 * 更新人
	 */
	@TableField("update_by")
    private String updateBy;

	/**
	 * 0正常、1已被删除
	 */
	@TableField("is_delete")
    private boolean isDelete;

}
