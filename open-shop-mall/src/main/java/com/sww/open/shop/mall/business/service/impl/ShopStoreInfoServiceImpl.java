package com.sww.open.shop.mall.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sww.open.shop.mall.business.mapper.ShopStoreInfoMapper;
import com.sww.open.shop.mall.business.entity.ShopStoreInfo;
import com.sww.open.shop.mall.business.service.ShopStoreInfoService;
import org.springframework.stereotype.Service;


/**
 * 店铺表
 *
 * @author sww
 * @date 2021-08-15 20:11:51
 */
@Service("shopStoreInfoService")
public class ShopStoreInfoServiceImpl extends ServiceImpl<ShopStoreInfoMapper, ShopStoreInfo> implements ShopStoreInfoService {

}
