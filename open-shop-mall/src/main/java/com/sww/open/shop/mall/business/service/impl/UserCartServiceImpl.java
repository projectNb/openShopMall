package com.sww.open.shop.mall.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sww.open.shop.mall.business.mapper.UserCartMapper;
import com.sww.open.shop.mall.business.entity.UserCart;
import com.sww.open.shop.mall.business.service.UserCartService;
import org.springframework.stereotype.Service;


/**
 * 用户购物车表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@Service("userCartService")
public class UserCartServiceImpl extends ServiceImpl<UserCartMapper, UserCart> implements UserCartService {

}
