package com.sww.open.shop.mall.business.mapper;

import com.sww.open.shop.mall.business.entity.ProductInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 餐品表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@Mapper
public interface ProductInfoMapper extends BaseMapper<ProductInfo> {
	
}
