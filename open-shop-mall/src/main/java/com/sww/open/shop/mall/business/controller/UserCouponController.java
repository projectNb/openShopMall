package com.sww.open.shop.mall.business.controller;

import com.sww.open.shop.mall.business.entity.UserCoupon;
import com.sww.open.shop.mall.business.service.UserCouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 * 用户优惠卷表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@RestController
@RequestMapping("/business/usercoupon")
public class UserCouponController {

    @Autowired
    private UserCouponService userCouponService;

    /***
     * 查询一条数据
     * @param entity
     * @return
     */
    @RequestMapping(path = "/get", method = RequestMethod.POST)
    public Object query(@RequestBody UserCoupon entity) {

        return null;
    }

    /***
     * 新增数据
     * @param entity
     * @return
     */
    @RequestMapping(path = "/add", method = RequestMethod.POST)
    public Object add(@RequestBody UserCoupon entity) {

        return null;
    }

    /**
     * 更新数据
     *
     * @param entity
     * @return
     */
    @RequestMapping(path = "/update", method = RequestMethod.POST)
    public Object update(@RequestBody UserCoupon entity) {

        return null;
    }
}
