package com.sww.open.shop.mall.business.service;

import com.sww.open.shop.mall.business.entity.QrCodeInfo;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * 门店二维码表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
public interface QrCodeInfoService extends IService<QrCodeInfo> {

}

