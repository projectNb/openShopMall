package com.sww.open.shop.mall.business.entity;

import lombok.*;
import java.util.Date;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.*;


/**
 * 餐品分类表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@TableName("tb_product_category_info")
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ProductCategoryInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 分类ID
	 */
    @TableId
	@TableField("category_id")
    private Long categoryId;

	/**
	 * 分类名称
	 */
	@TableField("category_mame")
    private String categoryMame;

	/**
	 * 分类排序: 数字越小、排名越靠前
	 */
	@TableField("sequence")
    private Long sequence;

	/**
	 * 分类描述
	 */
	@TableField("category_desc")
    private String categoryDesc;

	/**
	 * 0普通分类、1必选分类
	 */
	@TableField("category_mode")
    private boolean categoryMode;

	/**
	 * 0禁用、1启用
	 */
	@TableField("category_status")
    private boolean categoryStatus;

	/**
	 * 店铺id
	 */
	@TableField("shop_id")
    private Long shopId;

	/**
	 * 分类贩卖时间段(可改)
	 */
	@TableField("product_time")
    private String productTime;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
    private Date createTime;

	/**
	 * 创建人
	 */
	@TableField("create_by")
    private String createBy;

	/**
	 * 更新时间
	 */
	@TableField("update_time")
    private Date updateTime;

	/**
	 * 更新人
	 */
	@TableField("update_by")
    private String updateBy;

	/**
	 * 0正常、1已被删除
	 */
	@TableField("is_delete")
    private boolean isDelete;

}
