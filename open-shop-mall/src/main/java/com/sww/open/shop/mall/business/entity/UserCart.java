package com.sww.open.shop.mall.business.entity;

import lombok.*;
import java.util.Date;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.*;


/**
 * 用户购物车表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@TableName("tb_user_cart")
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class UserCart implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户id
	 */
    @TableId
	@TableField("user_cart_id")
    private Long userCartId;

	/**
	 * 用户id
	 */
	@TableField("user_id")
    private Long userId;

	/**
	 * 购物车内容
	 */
	@TableField("user_cart")
    private String userCart;

	/**
	 * 优惠卷id
	 */
	@TableField("user_coupon_id")
    private String userCouponId;

	/**
	 * 是否已经支付：0-未支付，1-已支付
	 */
	@TableField("is_payed")
    private boolean isPayed;

	/**
	 * 订单总金额(单位：分)
	 */
	@TableField("gross_total")
    private Integer grossTotal;

	/**
	 * 订单补贴金额
	 */
	@TableField("subsidy")
    private Integer subsidy;

	/**
	 * 餐单金额(单位：分)
	 */
	@TableField("product_price")
    private Integer productPrice;

	/**
	 * 运送费(单位：分)
	 */
	@TableField("deliveryfee")
    private Integer deliveryfee;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
    private Date createTime;

	/**
	 * 创建人
	 */
	@TableField("create_by")
    private String createBy;

	/**
	 * 更新时间
	 */
	@TableField("update_time")
    private Date updateTime;

	/**
	 * 更新人
	 */
	@TableField("update_by")
    private String updateBy;

	/**
	 * 0正常、1已被删除
	 */
	@TableField("is_delete")
    private boolean isDelete;

}
