package com.sww.open.shop.mall.business.entity;

import lombok.*;
import java.util.Date;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.*;


/**
 * 餐品表
 *
 * @author sww
 * @date 2021-08-15 20:11:25
 */
@TableName("tb_product_info")
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ProductInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 餐品ID
	 */
    @TableId
	@TableField("product_id")
    private Long productId;

	/**
	 * 分类ID
	 */
	@TableField("category_id")
    private Long categoryId;

	/**
	 * 餐品名称(可改)
	 */
	@TableField("product_mame")
    private String productMame;

	/**
	 * 店铺id
	 */
	@TableField("shop_id")
    private Long shopId;

	/**
	 * 打包盒数量
	 */
	@TableField("box_num")
    private Integer boxNum;

	/**
	 * 打包盒价格
	 */
	@TableField("box_price")
    private double boxPrice;

	/**
	 * 菜品上下架状态（用户端），0表上架，1表下架
	 */
	@TableField("is_sold_out")
    private boolean isSoldOut;

	/**
	 * 最小购买量，最小为1
	 */
	@TableField("min_order_count")
    private boolean minOrderCount;

	/**
	 * 菜品描述，最多不超过200字
	 */
	@TableField("product_desc")
    private String productDesc;

	/**
	 * 菜品价格，不能为负数
	 */
	@TableField("product_price")
    private double productPrice;

	/**
	 * 单位
	 */
	@TableField("product_unit")
    private String productUnit;

	/**
	 * 菜品图片ids
	 */
	@TableField("picture_list")
    private String pictureList;

	/**
	 * 分类排序: 数字越小、排名越靠前
	 */
	@TableField("picture_sequence")
    private Long pictureSequence;

	/**
	 * 餐品贩卖时间段(可改)
	 */
	@TableField("product_time")
    private String productTime;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
    private Date createTime;

	/**
	 * 创建人
	 */
	@TableField("create_by")
    private String createBy;

	/**
	 * 更新时间
	 */
	@TableField("update_time")
    private Date updateTime;

	/**
	 * 更新人
	 */
	@TableField("update_by")
    private String updateBy;

	/**
	 * 0正常、1已被删除
	 */
	@TableField("is_delete")
    private boolean isDelete;

}
